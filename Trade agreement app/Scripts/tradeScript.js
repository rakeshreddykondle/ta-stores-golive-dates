﻿
$(document).ready(function () {
    var BranchList = [],RulesList=[];
    LoadBranchList();

    var UserRole = $('#UserRole').val().toLowerCase();
    $("form").submit(function (e) {
        e.preventDefault();
        return false;
    });

    $('#goLive_date').datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: 1,
        maxDate:'+2y',
        changeMonth: true,
        onSelect: function (date) {
            var gl = date.split('/');
            var selectedDate = new Date(gl[1] + '/' + gl[0] + '/' + gl[2]);
            var msecsInADay = 86400000;
            var endDate = new Date(selectedDate.getTime() + msecsInADay);

            //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
            $("#Inactive_date").datepicker("option", "minDate", endDate);

        }
        
    });

    $('#Inactive_date').datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: 2,
        maxDate: '+2y',
        changeMonth: true,
        onSelect: function (date) {
            var gl = date.split('/');
            var selectedDate = new Date(gl[1] + '/' + gl[0] + '/' + gl[2]);
            var msecsInADay = 86400000;
            var endDate = new Date(selectedDate.getTime() - msecsInADay);

            //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
            //$("#goLive_date").datepicker("option", "minDate", '1');
            $("#goLive_date").datepicker("option", "maxDate", endDate);

        }

    });

    

    //Load Stores List
    function LoadBranchList() {
        $.ajax({
            cache: false,
            async: false,
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/GetBranchList",
            dataType: "json",
            success: function (res) {
                //console.log(res);
                if (res.data && res.data.length) {
                    BranchList = res.data;
                    var branchStr = '';
                    $.each(res.data, function (k, v) {
                        branchStr += '<option value=' + v.BranchID + '>' + (v.BranchID +' - '+v.Storename +' - '+v.AxAccountCode) + '</option>';
                    });
                    $('.ams_stores select').html(branchStr);
                    $('.ams_stores select').multiselect({
                        numberDisplayed: 1,
                        enableCaseInsensitiveFiltering: true,
                        maxHeight: 250
                    });
                }
            },
            error: function (result, xhr) {
            }
        });
    }



    

    $('.close-modal').click(function () {
        jQuery("#myGrid").jqGrid("clearGridData");
        $('#new-custom-pg-items').empty();
        $('#new-custom-pg,#custom-pg').val('');
        $('.duplicate-id-section').hide();
        duplicatePassThrough = true;
        $('.save-pg').removeClass('create-new-cg').removeClass('edit-cg');
        $("#myModal").modal('hide');
        $('#clear_td').text('Clear');
    });

  


    function loadPriorityTable(reload) {

        $.ajax({
            cache: false,
            async: false,
            cors: true,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "/home/GetTradeStoresList",
            dataType: "json",
            success: function (res) {
                RulesList = res.data;
                if (reload) {
                    $('.priority-table').DataTable().destroy();
                    $('.priority-table tbody').empty();
                }

                var str = '';
                $.each(res.data, function (k, v) {
                    str += '<tr class="Info"><td>' + v.BranchID + '</td><td class="store-selected">' + getBranchName(v.BranchID) + '</td><td>' + dateFn(v.GoLiveDateTime) + '</td><td>' + (v.InactiveDateTime != null ? dateFn(v.InactiveDateTime) : '') + '</td>' +
                        '<td class="action-icons">' + ((UserRole == 'admin') ? (ValidateDateFn(v.InactiveDateTime)? '<span class="glyphicon glyphicon-pencil edit-td" data-go-live-date=' + v.GoLiveDateTime + ' data-inactive-date=' + v.InactiveDateTime + '></span>':'') +
                            (ValidateDateFn(v.GoLiveDateTime) ? '<span class="glyphicon glyphicon-trash delete-td" data-id=' + v.BranchID + '></span>' : '') : '') + '</td></tr>';
                });
                $('.priority-table tbody').html(str);
                var priorityTable = $('.priority-table').DataTable({
                    //bFilter: false,
                    scrollY: '55vh',
                    scrollCollapse: true,
                    pageLength: 25
                });
                priorityTable.columns.adjust();      
                $('.loading-bar-section').hide();
            },
            error: function (result, xhr) {
                $('.loading-bar-section').hide();
            }
        });
       
    }
    loadPriorityTable();


    function dateFn(dt) {
        var d = new Date(dt.match(/\d+/)[0] * 1),
            minutes = d.getMinutes().toString().length == 1 ? '0' + d.getMinutes() : d.getMinutes(),
            hours = d.getHours().toString().length == 1 ? '0' + d.getHours() : d.getHours(),
            ampm = d.getHours() >= 12 ? 'pm' : 'am',
            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        return days[d.getDay()] + ' ' + months[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear() + ' ' + hours + ':' + minutes + ampm;

        
    }


    function ValidateDateFn(dt) {
        if (!dt) {
            return true;
        }
        var d = new Date(dt.match(/\d+/)[0] * 1),today = new Date();

        if (d > today) {
            return true;
        } else {
            return false;
        }
    }


    function getBranchName(storeType) {

        var bName = '';
        BranchList.forEach(function (v) {
            if (v.BranchID == storeType) {
                bName = v.Storename;
            }
        });
        return bName;
        
    }

    
    function populateDateFn(dt) {
        if (!dt || dt == 'null') {
            return null;
        }
        var now = new Date(dt.match(/\d+/)[0] * 1);
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);

        var ret_dt = (day) + "/" + (month) + "/" + now.getFullYear();
        return ret_dt;
    }


    //Edit the existing Trade agreement rule
    $('.priority-table tbody').on('click', '.edit-td', function () {
        $('#clear_td').text('Close');
        $('.inactiveDate').css('visibility', 'visible');
        $('.ta-form').addClass('activeEditForm');
        $('.ta-form').nextAll().css('pointer-events', 'none');
        var store_type = $(this).parents('tr').find('.store-selected').text().toLowerCase();
        
        $(".ams_stores select option").prop("selected", false);
        $("#few_stores select").multiselect("refresh").multiselect('disable');


        var GoliveDate = populateDateFn($(this).attr('data-go-live-date')); 
        var InactiveDate = populateDateFn($(this).attr('data-inactive-date')); 

        //$('#goLive_date').val(GoliveDate);

        var gl = GoliveDate.split('/');
        var selectedDate = new Date(gl[1] + '/' + gl[0] + '/' + gl[2]);
        var msecsInADay = 86400000;
        var minDate = new Date(selectedDate.getTime() + msecsInADay);
        var minDate_goLive = new Date(selectedDate.getTime());



        
        if (minDate_goLive > new Date()) {
            $("#goLive_date").datepicker("option", "minDate", 1).prop('disabled', false);
            $("#Inactive_date").datepicker("option", "minDate", minDate);
        } else {
            
            $("#goLive_date").datepicker("option", "minDate", minDate_goLive).prop('disabled', true);
            $("#Inactive_date").datepicker("option", "minDate", 0);
        }
        
        $("#goLive_date").datepicker("setDate", GoliveDate);
        $("#Inactive_date").datepicker("setDate", InactiveDate);
        

        var brId = BranchList.find(function (v) {
            return v.Storename.toLowerCase() == store_type;
        }).BranchID;
        $("#few_stores select option[value='" + brId + "']")
            .prop("selected", true);
        $("#few_stores select").multiselect("refresh");


        
        $('.priority-table tbody tr').removeClass('active-row');
        $(this).parents('tr').addClass('active-row');
        

        $("html, body").animate({ scrollTop: "0px" });
    });
    //Edit the existing Trade agreement rule
    $('.priority-table tbody').on('click', '.delete-td', function () {
        console.log($(this).attr('data-id'));
        if (confirm("Are you sure you want to delete a Date?")) {
            $.ajax({
                cors: true,
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/home/DeleteTradeRule?Id=" + $(this).attr('data-id'),
                dataType: "json",
                success: function (res) {
                    showNotification(res.data, 'success','center')
                    loadPriorityTable(true);
                },
                error: function (result, xhr) {
                    $('.loading-bar-section').hide();
                }
            });
        } else {
            
        }
    });
    


    $('#clear_td').click(function () {
        $(".ams_stores select option").prop("selected", false);
        $("#few_stores select").multiselect("refresh").multiselect('enable');;
        $('.priority-table tr').removeClass('active-row');
        $('.ta-form').removeClass('activeEditForm');
        $('.ta-form').nextAll().css('pointer-events', 'all');
        $("#goLive_date").datepicker("setDate", '').prop("disabled", false);
        $("#Inactive_date").datepicker("setDate", '');
        $('.inactiveDate').css('visibility', 'hidden');
        $('#clear_td').text('Clear');
    });



    function showNotification(msg, type, align) {
      return $.bootstrapGrowl(msg, {
                ele: 'body',
                type: type,
                offset: { from: 'top', amount: 10 },
                align: align ? align: 'center',
                width: type == 'success'? 1000: 500,
                delay: 6000,
                allow_dismiss: true,
                stackup_spacing: 10
            });
    }





    $('#save_td').click(function () {
        
        var selected_stores = $('#few_stores select option:selected').map(function (a, item) { return parseInt(item.value); }).toArray();
        var goLive_date = $('#goLive_date').val();
        var inactive_date = $('#Inactive_date').val();
        console.log('before::', selected_stores);
        console.log('before::', goLive_date);
        if (!selected_stores.length) {
            alert('Please select stores');
            return;
        }

        var WarningMsg = '';
        if (!$('.ta-form').hasClass('activeEditForm')) {
            
            RulesList.forEach(function (v) {
                
                if (selected_stores.indexOf(v.BranchID)>-1) {
                    if (WarningMsg == '') {
                        WarningMsg = 'Below stores are already created. Please un select them.\n';
                    }
                    WarningMsg += getBranchName(v.BranchID)+'\n'
                }
            }); 
        }

        if (WarningMsg) {
            alert(WarningMsg);
            return;

        }

        if (!goLive_date) {
            alert('Please select go live date');
            return;
        }
        
        var gl = goLive_date.split('/'), incDt = [], InactiveDateTime = null;
        var GoLiveDateTime = new Date(gl[1] + '/' + gl[0] + '/' + gl[2]);
        if (inactive_date) {
            incDt = inactive_date.split('/');
            InactiveDateTime =  new Date(incDt[1] + '/' + incDt[0] + '/' + incDt[2]);
        }
        console.log('after::',selected_stores);
        console.log('after::', goLive_date);
        var TradeStoreList = [];
        selected_stores.forEach(function (v) {
            var TradeStoreObj = {};
            TradeStoreObj.BranchID = v;
            TradeStoreObj.GoLiveDateTime = GoLiveDateTime;
            TradeStoreObj.InactiveDateTime = InactiveDateTime; //incDt.length?new Date(incDt[1] + '/' + incDt[0] + '/' + incDt[2]):null;
            TradeStoreList.push(TradeStoreObj);
        });


        if ($('#goLive_date').prop('disabled') == false && GoLiveDateTime<= new Date()) {
            alert('Please select future go live date');
            return;
        }

        if (InactiveDateTime != null) {
            if (InactiveDateTime > GoLiveDateTime && InactiveDateTime > new Date()) {
                
            } else {
                alert('Please select future inactive date');
                return;
            }
        }
        

        $('.loading-bar-section').show();
        $.ajax({
            cors: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/home/SaveTradeStoreRule",
            dataType: "json",
            data: JSON.stringify(TradeStoreList),
            success: function (res) {
                console.log(res);
                $('.loading-bar-section').hide();
                loadPriorityTable(true);

                showNotification(res.data, 'success', 'center');
                $('.ta-form').removeClass('activeEditForm');
                $('.ta-form').nextAll().css('pointer-events', 'all');
                $("#goLive_date").datepicker("setDate", '').prop('disabled', false);
                $("#Inactive_date").datepicker("setDate", '');
                $(".ams_stores select option").prop("selected", false);
                $("#few_stores select").multiselect("refresh").multiselect('enable');
                $('.inactiveDate').css('visibility', 'hidden');

                $('#clear_td').text('Clear');
            },
            error: function (result, xhr) {
                $('.loading-bar-section').hide();
                $('.ta-form').removeClass('activeEditForm');
                $('.ta-form').nextAll().css('pointer-events', 'all');
                $("#goLive_date").datepicker("setDate", '').prop('disabled', false);
                $("#Inactive_date").datepicker("setDate", '');
                $(".ams_stores select option").prop("selected", false);
                $("#few_stores select").multiselect("refresh").multiselect('enable');
                $('.inactiveDate').css('visibility', 'hidden');

                $('#clear_td').text('Clear');
            }
        });
    });

    
});











    









