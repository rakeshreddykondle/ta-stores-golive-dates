﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trade_agreement_app.Models;
using System.Data.Entity;
using System.Net.NetworkInformation;
using System.IO;
using System.DirectoryServices.AccountManagement;

namespace Trade_agreement_app.Controllers
{
    public class HomeController : Controller
    {
        readonly CWMgtStoreInvoicesEntities TradeStoreContext = new CWMgtStoreInvoicesEntities();
        readonly General_ReferenceEntities GeneralContext = new General_ReferenceEntities();
        readonly string TA_AdminType = System.Configuration.ConfigurationManager.AppSettings["TA_AdminType"];
        readonly string TA_UserType = System.Configuration.ConfigurationManager.AppSettings["TA_UserType"];
        public ActionResult Index()
        {
            UserListEnity CurrentUser = GetUserList(TA_AdminType, "admin");
            if(CurrentUser == null)
            {
                CurrentUser = GetUserList(TA_UserType, "user");
            }

            if(CurrentUser != null)
            {
                Session["Name"] = CurrentUser.Name;
                Session["GroupName"] = CurrentUser.GroupName;
                Session["UserRole"] =  CurrentUser.UserRole;
                Session["EmailAddress"] = CurrentUser.EmailAddress;
            }
            else
            {
                Session["Name"] = null;
                Session["GroupName"] = null;
                Session["UserRole"] = null;
                Session["EmailAddress"] = null;
            }
            return View();
        }

        public UserListEnity GetUserList(string GrouName,string UserRole)
        {
            //var CurrentUser = UserPrincipal.Current.EmailAddress;
            List<UserListEnity> UserList = new List<UserListEnity>();
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain);

            UserPrincipal userTT = UserPrincipal.FindByIdentity(ctx, System.Environment.UserName);
            var CurrentUser = userTT.EmailAddress;
            using (var context = new PrincipalContext(ContextType.Domain, "mychemist"))
            {
                using (var group = GroupPrincipal.FindByIdentity(context, GrouName))
                {
                    if (group == null)
                    {
                        //MessageBox.Show("Group does not exist");
                    }
                    else
                    {
                         var users = group.GetMembers(true);
                        foreach (UserPrincipal user in users)
                        {
                            if(user.EmailAddress == CurrentUser)
                            {
                                UserListEnity UserEnity = new UserListEnity
                                {
                                    Name = user.Name,
                                    EmailAddress = user.EmailAddress,
                                    UserRole = UserRole,
                                    GroupName = group.Name
                                };
                                UserList.Add(UserEnity);
                                
                            }
                            //user variable has the details about the user 
                            
                        }
                    }
                }

                
            }
            return UserList.FirstOrDefault() ;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public JsonResult GetBranchList()
        {
            //List<int> branches = d.Split(',').Select(Int32.Parse).ToList();
            var res = GeneralContext.BranchInfoGlobals.Where(x=>(x.StoreGroupID !=0 && x.CountryCode == "AUS")).ToList();
            //var res1 =  (from s in StockContext.BranchInfoes where !branches.Contains(s.BranchID) select s).ToList();
            return Json(new { data = res }, JsonRequestBehavior.AllowGet);
        }

        
       

        


        public JsonResult GetTradeStoresList()
        {
            List<TradeAgreementStore> TradeStoreList = TradeStoreContext.TradeAgreementStores.ToList();
            return Json(new { data = TradeStoreList.OrderByDescending(x => x.GoLiveDateTime) }, JsonRequestBehavior.AllowGet);
        }


       
        public JsonResult DeleteTradeRule(int Id)
        {
            

            var RemovePriorityRule = TradeStoreContext.TradeAgreementStores.Where(x => x.BranchID == Id).FirstOrDefault();
            TradeStoreContext.TradeAgreementStores.Remove(RemovePriorityRule);
            TradeStoreContext.SaveChanges();
            return Json(new { data = "Store has been successfully deleted." }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SaveTradeStoreRule(List<TradeAgreementStore> TradeStores)
        {
            string res = "";
            var domainName = IPGlobalProperties.GetIPGlobalProperties().DomainName.ToUpper();
            var domain = domainName.Substring(0, domainName.IndexOf('.'));
            string CreatedBy = "";
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                CreatedBy = System.Web.HttpContext.Current.User.Identity.Name;
            }
            else
            {
                CreatedBy = Path.Combine(domain, Environment.UserName);
            }

            foreach (var ts in TradeStores)
            {
                var TsRule = TradeStoreContext.TradeAgreementStores.Where(x => x.BranchID == ts.BranchID).FirstOrDefault();

                if (TsRule != null)
                {
                    TsRule.LastModifiedDateTime = DateTime.Now;
                    TsRule.LastModifiedBy = CreatedBy;
                    TsRule.CreatedBy = TsRule.CreatedBy != null ? TsRule.CreatedBy : CreatedBy;
                   
                    TsRule.InactiveDateTime = ts.InactiveDateTime;

                    if (TsRule.GoLiveDateTime > DateTime.Now)
                    {
                        //res = res != "" ? res : "No Changes to Save.";
                        TsRule.GoLiveDateTime = ts.GoLiveDateTime;
                    }
                    
                    TradeStoreContext.Entry(TsRule).State = EntityState.Modified;
                    res = "TA Store Dates- successfully updated!";

                }
                else
                {

                    ts.CreatedBy = CreatedBy;
                    ts.CreatedDateTime = DateTime.Now;
                    ts.LastModifiedDateTime = DateTime.Now;
                    ts.LastModifiedBy = CreatedBy;
                    TradeStoreContext.Entry(ts).State = EntityState.Added;
                    res = "TA Store Dates- successfully updated!";
                }
                

                
                
                

            }
            if(res == "TA Store Dates- successfully updated!")
            {
                TradeStoreContext.SaveChanges();
            }
            //var RemovePriorityRule = TradeStoreContext.TradeAgreementStores.Where(x => x.BranchID == Id).FirstOrDefault();
            //TradeStoreContext.TradeAgreementStores.Remove(RemovePriorityRule);
            //TradeStoreContext.SaveChanges();
            return Json(new { data = res}, JsonRequestBehavior.AllowGet);
        }




        public class UserListEnity
        {
            public string Name { get; set; }
            public string EmailAddress { get; set; }
            public string GroupName { get; set; }
            public string UserRole { get; set; }

        }


    }
}